%-------------------------------------------------------------------------
% -- SIZER
%-------------------------------------------------------------------------
%    Size wing by static analysis
%-------------------------------------------------------------------------
%--------------------------- N O T E S -----------------------------------
%    *Coordinate system
%
%    Rib plane is in xy plane
%    y is lift direction and x is oriented to leading edge direction
%    z direction is in spanwise direction leaded to right wing tip
%    0zyx: Aerodynamic center
%          
%    *Rib boom idealization labels
%
%          4---------
%      -      |      ---
%   2 -       |           --6
%   |  cell 1 |  cell 2     |
%   |         |          -- 5  
%   1 -       |      ---
%      -  3---------
%   
%   Most airfoils are antisymmetric. Here, we suppose they aren't, and to
%   estimate their symmetric coordinates this code finds equivalent
%   inertia moment symetrical coordinates
%   
%   *Sizing
%   
%   This model treat carbon tube as just axial stress supporter and
%   shear stress is supported by balsa only
%-------------------------------------------------------------------------
%% -- starter 'main for' variables
%-------------------------------------------------------------------------
r3   = zeros(nribs-1,1);
r4   = zeros(nribs-1,1);
td   = zeros(nribs-1,1);
ie   = zeros(nribs-1,1);
sigg = zeros(nribs-1,1);
tal  = zeros(4,2);
qt   = zeros(4,2);
talm = zeros(1,nribs-1);
k    = 0;
%-------------------------------------------------------------------------
%% -- running throught wing
%-------------------------------------------------------------------------
for section=1:nribs-1      % main for: run through all wing sections
    jb=section+1;
    %-------------------------------------------------------------------------
    % -- Geometry
    %-------------------------------------------------------------------------
    ch        = geo.g.ct + (geo.g.cr-geo.g.ct)*(geo.g.bw-rib_position(jb-1))/geo.g.bw;
    [x0a,y0a,x,y]=xyfoil(section, geo, rib_position);
    x0a       = x0a*ch;
    y0a       = y0a*ch;
    x13      = x0a(3) - x0a(1);
    x35      = x0a(5) - x0a(3);
    y0a       = -[y0a(2); -y0a(2); y0a(4); -y0a(4); y0a(6) ;-y0a(6) ];
    x0a       = [x0a(1); x0a(1); x0a(3); x0a(3); x0a(5); x0a(5) ];
%     jj=0;

    td(jb-1) = .0015;                       % min balsa thick           [m]
    tc       = .0005;                       % standard carbon tub thick [m]
    tal      = 1e10;                        % starter conditional while variable
     
        
        
    while (max(max(abs(tal)))>geo.m.tal_a || ie(section)>=1)  % first for: run while balsa is failing
    %-------------------------------------------------------------------------
    % -- Prior inertia moments
    %-------------------------------------------------------------------------
        %-------------------------------------------------------------------------
    % -- Booms
    %-------------------------------------------------------------------------
    b12      =  abs(y0a(1))+y0a(2);
    b13      =  sqrt( ( x0a(3) - x0a(1) )^2 + ( y0a(3) - y0a(1) )^2 )  ;
    b21      =  b12;
    b24      =  sqrt( ( x0a(4) - x0a(2) )^2 + ( y0a(4) - y0a(2) )^2 )  ;
    b31      =  b13;
    b34      =  abs(y0a(3))+y0a(4);
    b35      =  sqrt( ( x0a(5) - x0a(3) )^2 + ( y0a(5) - y0a(3) )^2 ) ;
    b42      =  b24;
    b43      =  b34;
    b46      =  sqrt( ( x0a(6) - x0a(4) )^2 + ( y0a(6) - y0a(4) )^2 ) ;
    b53      =  b35;
    b56      =  abs(y0a(5))+y0a(6);
    b65      =  b56;
    b64      =  b46;   
 
     b(1)       = y0a(1)^2*td(jb-1)/6*( b12*( y0a(2)/y0a(1) + 2 ) + b13*...
            ( y0a(3)/y0a(1) + 2 ) );
        b(2)       = y0a(2)^2*td(jb-1)/6*( b21*( y0a(1)/y0a(2) + 2 ) + b24*...
            ( y0a(4)/y0a(2) + 2 ) );
        b(3)       = y0a(3)^2*1/6*( td(jb-1) * ( b31*( y0a(1)/y0a(3) + 2 ) + b35 * td(jb-1) * ...
            ( y0a(5)/y0a(3) ) )  + b34*td(jb-1)*(y0a(4)/y0a(3) + 2 ) ) ;
        b(4)       = y0a(4)^2*1/6*( td(jb-1) * ( b42*( y0a(2)/y0a(4) + 2 ) + b46 * td(jb-1) * ...
            ( y0a(6)/y0a(4) ) )  + b43*td(jb-1)*(y0a(3)/y0a(4) + 2 ) ) ;
        b(5)       = y0a(5)^2*td(jb-1)/6*(b53*(y0a(3)/y0a(5) + 2) + b56*...
            (y0a(6)/y0a(5) + 2));
        b(6)       = y0a(6)^2*td(jb-1)/6*(b64*(y0a(4)/y0a(6) + 2) + b65*...
            (y0a(5)/y0a(6) + 2));
        c          = zeros(6,1);
        c(3)       = y0a(3)^2 * 2*pi*tc;
        c(4)       = y0a(4)^2 * 2*pi*tc;
        d          = sum(b(:));
    % Following variables find equivalent x symmetric coordinates to maintain innertia moment
        Ixx_t      = (d+c(3)*r3(jb-1)+c(4)*r4(jb-1)) ;                      % innertia moment [m4]  
    %-------------------------------------------------------------------------
   % -- New booms
   %-------------------------------------------------------------------------
 
        Ixx_s      = 2* sum(b(2:2:6)+ c(3)*r3(jb-1));
    %-------------------------------------------------------------------------
    % -- New equivalent inertia moments coordinates
    %-------------------------------------------------------------------------
        v          = sqrt ( Ixx_t / (Ixx_s) );
        yy0        = y0a(2:2:6)*v;
        xx0        = x0a(2:2:6)*v;  
        x13        = xx0(2) - xx0(1);
        x35        = xx0(3) - xx0(2);
        y0         = [-yy0(1); yy0(1); -yy0(2); yy0(2); -yy0(3) ;yy0(3) ];
        x0         = [xx0(1); xx0(1); xx0(2); xx0(2); xx0(3); xx0(3) ];
        
        
        
                b12        =  2*yy0(1);
        b21        =  b12;
        b13        =  sqrt( ( xx0(2) - xx0(1) )^2 + ( yy0(2) - yy0(1) )^2 )  ;
        b31        =  b13;
        b24        =  b13;
        b42        =  b24;
        b35        =  sqrt( ( xx0(3) - xx0(2) )^2 + ( yy0(3) - yy0(2) )^2 ) ;
        b53        =  b35;
        b34        =  2*yy0(2);
        b43        =  b34;
        b46        =  b35;
        b64        =  b46;
        b56        =  yy0(3)*2;
        b65        =  b56;
       

        b(1)       = y0(1)^2*td(jb-1)/6*( b12*( y0(2)/y0(1) + 2 ) + b13*...
            ( y0(3)/y0(1) + 2 ) );
        b(2)       = y0(2)^2*td(jb-1)/6*( b21*( y0(1)/y0(2) + 2 ) + b24*...
            ( y0(4)/y0(2) + 2 ) );
        b(3)       = y0(3)^2*1/6*( td(jb-1) * ( b31*( y0(1)/y0(3) + 2 ) + b35 * td(jb-1) * ...
            ( y0(5)/y0(3) ) )  + b34*td(jb-1)*(y0(4)/y0(3) + 2 ) ) ;
        b(4)       = y0(4)^2*1/6*( td(jb-1) * ( b42*( y0(2)/y0(4) + 2 ) + b46 * td(jb-1) * ...
            ( y0(6)/y0(4) ) )  + b43*td(jb-1)*(y0(3)/y0(4) + 2 ) ) ;
        b(5)       = y0(5)^2*td(jb-1)/6*(b53*(y0(3)/y0(5) + 2) + b56*...
            (y0(6)/y0(5) + 2));
        b(6)       = y0(6)^2*td(jb-1)/6*(b64*(y0(4)/y0(6) + 2) + b65*...
            (y0(5)/y0(6) + 2));
        c          = zeros(6,1);
        c(3)       = y0(3)^2 * 2*pi*tc;
        c(4)       = y0(4)^2 * 2*pi*tc;
   
        %-------------------------------------------------------------------------
        % -- Sizing CARBON TUBES - axial stress based on bending moment
        %-------------------------------------------------------------------------
        % r3 and r4 are equivalent carbon tube radious
        if k==0                         % second for: just runs one time each section
            g0  = 10000000000;
            g9  = g0;
            r4(jb-1) = 0;
            while ( g0>1/geo.m.FSc && g9>1/geo.m.FSc )
                r4(jb-1)   = r4(jb-1) + .0005;
                sigg(jb-1) = mx(section)*-yy0(2)/(d+c(3)*r4(jb-1)+c(4)*r4(jb-1));
                % Hill-Tsai criterion
                % geo.m.f0*sig_x^2 <1/FSc
                % geo.m.f9*sig_x^2  <1/FSc
                g0         = sqrt(geo.m.f0*sigg(jb-1)^2);    % Check out if 0� plies fail
                g9         = sqrt(geo.m.f9*sigg(jb-1)^2);    % Check out if 90� plies fail
                i            = i + 1;
            end
            r3(jb-1)=r4(jb-1);
        end
        %------------------------------------------------------------------------------------
        % -- Sizing balsa based on shear stress
        %------------------------------------------------------------------------------------ 
        % -- geometric properties
        %------------------------------------------------------------------------------------
        B        = (b(:)+c(:))./y0(:).^2;                               % boom area [m2]
        Ixx      = (d+c(3)*r3(jb-1)+c(4)*r4(jb-1));                     % innertia moment [m4]        
        A1       = x13*(yy0(2)+yy0(1));                                 % cell 1 area [m2]
        A2       = x35*(yy0(3)+yy0(2));                                 % cell 2 area [m2]
        %------------------------------------------------------------------------------------ 
        % -- direct forces
        %------------------------------------------------------------------------------------    
        f1       = mx(section)/Ixx;                                      % mf/Ixx
        pz       = f1.*y0(:).*B(:);                                      % direct force at z direction...
        %------------------------------------------------------------------------------------ 
        % -- taper angle:  x and y direction are related to actual
        % perpendicular and longitudinal direction related to carbon tube.
        % The following variables find angle of tape to calculate perp.
        % and long. forces related to carbon tube direcion
        %------------------------------------------------------------------------------------ 
        dydz(1)      =  (geo.g.ct-geo.g.cr)/geo.g.bw*-yy0(1);
        dydz(2)      =  (geo.g.ct-geo.g.cr)/geo.g.bw*+yy0(1);
        dydz(3)      =  (geo.g.ct-geo.g.cr)/geo.g.bw*-yy0(2);
        dydz(4)      =  (geo.g.ct-geo.g.cr)/geo.g.bw*+yy0(2);
        dydz(5)      =  (geo.g.ct-geo.g.cr)/geo.g.bw*-yy0(3);
        dydz(6)      =  (geo.g.ct-geo.g.cr)/geo.g.bw*+yy0(3);
        dxdz(1)      =  (geo.g.ct-geo.g.cr)/geo.g.bw*(xx0(1));
        dxdz(2)      =  (geo.g.ct-geo.g.cr)/geo.g.bw*(xx0(1));
        dxdz(3)      =   0;
        dxdz(4)      =   0;
        dxdz(5)      =  (geo.g.ct-geo.g.cr)/geo.g.bw*(-xx0(3));
        dxdz(6)      =  (geo.g.ct-geo.g.cr)/geo.g.bw*(-xx0(3));
        %------------------------------------------------------------------------------------ 
        % -- xy plan direct forces [N]
        %------------------------------------------------------------------------------------  
        py           =  pz(:).*dydz(:);                                           % [N]                           
        px           =  pz(:).*dxdz(:);                                           % [N] 
        %------------------------------------------------------------------------------------ 
        % -- moment arms relatd to Aerodynamic/Shear Center [m]
        %------------------------------------------------------------------------------------  
        yr(1) = -x13;
        yr(2) = -x13;
        nr(1) = -yy0(1);
        nr(2) = yy0(1);
        yr(3:4) = 0;
        nr(3) = -yy0(2);
        nr(4) = yy0(2);
        yr(5) = x35;
        yr(6) = x35;
        nr(5) = -yy0(3);
        nr(6) = yy0(3);
        
        pxn = px(:).*nr(:);                                                      % px moment [Nm]
        pye = py(:).*yr(:);                                                      % py moment [Nm]
        %------------------------------------------------------------------------------------ 
        %------------------------------------------------------------------------------------ 
        % -- shear flow [N/m]
        %------------------------------------------------------------------------------------ 
        %------------------------------------------------------------------------------------ 
        syw  = Lift(section) - sum(py);                                    % on plane y load [N]
        %------------------------------------------------------------------------------------ 
        % -- qbasic - open at 24 and 35 [N/m]
        %------------------------------------------------------------------------------------ 
        f2   = -syw/Ixx;
        byy   = B(:).*y0(:);                                                  
        % ------ cell 1
        qb21 = 0;
        qb13 = f2*byy(1);
        qb34 = qb13+f2*byy(3);
        qb42 = qb34+f2*byy(4);
        % ----- cell 2
        qb56 = 0;
        qb64 = f2*byy(6);
        qb43 = qb64+f2*byy(4);
        qb35 = qb43+f2*byy(3);
        % ----- int q*ds/t
        f3   = (qb21*b12+qb42*b24+qb34*b34+qb13*b13)/td(jb-1);              % cell 1
        f4   = (qb43*b34+qb64*b35+qb56*b56+qb35*b35)/td(jb-1);              % cell 2
        % ----- delta s/t*
        delta22 = (b56 + b46 + b34 + b35)/td(jb-1);
        delta21 = b34/td(jb-1);
        delta12 = b34/td(jb-1);
        delta11 = (b12 + 2*b13 + b34)/td(jb-1);
        %------------------------------------------------------------------------------------ 
        % -- Linear system: finding qs0i/qs0ii
        %------------------------------------------------------------------------------------
        
        %                  qs0i                 qs0ii
        A    = [ delta11/A1+delta12/A2 -delta21/A1-delta22/A2;            % twist
                       2*A1                          2*A2     ];          % web moments sum at 23 web
        
        bb   = [ -f3/A1+f4/A2 ;                                           % twist
               -my(section)-qb56*b56*x35-qb21*b12*x13-sum(pxn)-sum(pye)]; % web moments sum at 23 web
        
        q    = A\bb;                                                      % qs0i/qs0ii [N/m] 
        %------------------------------------------------------------------------------------ 
        % -- Total shear flow [N/m]
        %------------------------------------------------------------------------------------
        % ----- cell 1   
        qt(1,1,section) = qb21+q(1);        % q12
        qt(2,1,section) = q(1);             % q24
        qt(3,1,section) = qb34+q(1)-q(2);   % q43
        qt(4,1,section) = q(1);             % q31
        % ----- cell 2
        qt(1,2,section) = qb56+q(2);        % q65
        qt(2,2,section) =  q(2);             % q53
        qt(3,2,section) = qb43 + q(2)-q(1); % q34
        qt(4,2,section) = q(2);             % q46
        %------------------------------------------------------------------------------------ 
        % -- Webs shear load [Pa]
        %------------------------------------------------------------------------------------
        tal             = qt(:,:,section)/td(jb-1);
%         jj=jj+1;                     % rib station number 
        %------------------------------------------------------------------------------------ 
        % -- If balsa fail following variables increase dimension
        %------------------------------------------------------------------------------------
        buckling                     % buckling analysis
        if max(max(abs(tal)))>geo.m.tal_a 
            r3(jb-1) = r3(jb-1)+.0005;
            td(jb-1) = td(jb-1)+.00025;
            r4(jb-1) = r4(jb-1)+.0005;
            k       = 1;
        elseif ie(section)>=1
            td(jb-1) = td(jb-1)+.00025;
        end
    end
    k=0;
    talm(section) = abs(tal(3,1));
end

% figure(13)
% subplot(1,3,1)
% plot(rib_position(1:nribs-1)*1e3,1*r4*1e3,'ro')
% xlabel('span position [mm]');
% ylabel('Carbon tube radius [mm]');
% grid on
% subplot(1,3,2)
% plot(rib_position(1:nribs-1)*1e3,td*1e3,'bo')
% xlabel('span position [mm]');
% ylabel('Balsa thickness [mm]');
% grid on
% subplot(1,3,3)
% plot(rib_position(1:nribs-1)*1e3,ie,'bo')
% xlabel('span position [mm]');
% ylabel('Buckling index [-]');
% grid on
% 
% 
% 
% 
% 






