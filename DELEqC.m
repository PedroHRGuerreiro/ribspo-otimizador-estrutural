function xb = DELEqC(bw,rib_spacing,nribs)
c               = bw;
xn              = rib_spacing';
E               = ones(1,nribs);
P               = eye(nribs);
EP              = E*P;
B               = EP(1,1);
N               = [EP(2:nribs)];
xb              = inv(B)*c - inv(B)*N*xn;



