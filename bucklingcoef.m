function [ ks ] = bucklingcoef(dx, alength, t, r, nu,geo)
%   dx       - rib's spacing
%   alength  - upper camber length 
%   zb       - buckling parameter  b^2/(rt)*(1-nu^2)(1/2)
%   t        - plate thickness

%   This function outputs the buckling coefficient for curved plates
%   based on NASA's reference TECHNICAL NOTE 3783 (05 Paper) 

%-------------------------------------------------------------------------
%% -- Long plates
%-------------------------------------------------------------------------

w=dx/alength;
a           =  max(dx, alength);
b           =  min(dx, alength);
ki          =  a/b;
zb          =  b^2/(r*t)*(1-nu^2)^(1/2);

% if dx>alength
 if w>=1 
     if ki<1.5
     ks = polyval(geo.buckling.fkb1,zb);
     elseif ki>=1.5 && ki<2
         ks =  polyval(geo.buckling.fkb15,zb);
     elseif ki>=2 && ki<3
         ks = polyval(geo.buckling.fkb2, zb);
     elseif ki>=3 && ki<4
         ks = polyval(geo.buckling.fkb3, zb);
     elseif ki>=4
         ks = polyval(geo.buckling.fkbi, zb);
     else   
     end    
 end
 
  if w<1 
     if ki<1.5
     ks = polyval(geo.buckling.fkw1,zb);
     elseif ki>=1.5 && ki<2
         ks =  polyval(geo.buckling.fkw15,zb);
     elseif ki>=2 && ki<3
         ks = polyval(geo.buckling.fkw2, zb);
     elseif ki>=3 
         ks = polyval(geo.buckling.fkwi, zb);
     end    
 end




end

