%-------------------------------------------------------------------------
% -- Lift Torsor 
%-------------------------------------------------------------------------
%    Figures out each rib loads.
%-------------------------------------------------------------------------
% -- Geometry
%-------------------------------------------------------------------------
ccr         =   geo.g.cr + (geo.g.ct-geo.g.cr)*(geo.g.bw-rib_position)/geo.g.bw ;
af          =   length(Cm);
%-------------------------------------------------------------------------
% -- Each rib Cm and Cl 
%-------------------------------------------------------------------------
cm          =   spline(yya(1:af*.5),Cm(1:af*.5),rib_position(1:nribs-1));
cly         =   spline(yya(1:af*.5),Cl(1:af*.5),rib_position(1:nribs-1));
chord       =   flip(ccr);
%-------------------------------------------------------------------------
% -- Each rib Lift and bending moment
%-------------------------------------------------------------------------
for i=1:nribs-1
area(i)     =   (chord(i)+chord(i+1))*rib_spacing(i)*.5;
my(i)       =   -0.5*geo.fc.FC*geo.fc.rho*(geo.fc.vc^2)*area(i)*cm(i)*chord(i);
Lift(i)     =   0.5*geo.fc.FC*geo.fc.rho*(geo.fc.vc^2)*area(i)*cly(i)*chord(i);
end
%-------------------------------------------------------------------------
% -- Each rib torsor moment
%-------------------------------------------------------------------------
mx          =   zeros(nribs-1,1);

 my   =  ((0.5*geo.fc.FC*geo.fc.mtow*geo.fc.g)/(sum(Lift)))*my ;

 Lift = ((0.5*geo.fc.FC*geo.fc.mtow*geo.fc.g)/(sum(Lift)))*Lift ;
 
 
for ij=1:nribs-1
    
    for ik=ij:nribs-1
        
    mx(ij)  =   mx(ij) + Lift(ik)*abs((rib_position(ik)-rib_position(ij)));
    end
    
    
end





%-------------------------------------------------------------------------
% 
% figure
%  plot(rib_position(1:nribs-1),Lift,'r',rib_position(1:nribs-1),my,'b')