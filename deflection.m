%-------------------------------------------------------------------------
% -- deflection
%-------------------------------------------------------------------------
%    Estimate wing tip deflection using analytical method
%-------------------------------------------------------------------------
%-------------------------------------------------------------------------
% -- Each section moment equation
%-------------------------------------------------------------------------
syms z
a       = flip(geo.g.bw-rib_position);
mo      = sym(zeros(nribs-1, 1));
for i=1:nribs-1
    mo(i) = Lift(nribs-i)*(z-a(i));             % bending moment function
end
%-------------------------------------------------------------------------
% -- Deflection due to x moment - mdef
%-------------------------------------------------------------------------
imo     = sym(zeros(nribs-1, 1));
mdef    = 0;
Eavg    = (geo.m.El_carbontube + 2*geo.m.Ex )/3;            % average E 
Gavg    = (geo.m.Glt_carbontube + 2*geo.m.Gxy )/3;          % average E 

for ij=1:nribs-1
    for i=1:nribs-1
        imo(i,ij) = mo(i)*isAlways((a(ij)-a(i))>=0);
    end
    mdef = mdef + int(sum(imo(:,ij))*z,z,[a(ij) a(ij+1)])/(Eavg*Ixx);  
end
%-------------------------------------------------------------------------
% -- Deflection due to shear load - sdeaf
%-------------------------------------------------------------------------
H12     = qt(1,1,section)^2*b12;
H24     = qt(2,1,section)^2*b24;
H43     = qt(3,1,section)^2*b34;
H31     = qt(4,1,section)^2*b13;
H65     = qt(1,2,section)^2*b56;
H53     = qt(2,2,section)^2*b35;
H46     = qt(4,2,section)^2*b46;
sdef    = (H12 + H24 + H43 + H31 + H65 + H53 + H46)*...
    geo.g.bw/(Gavg*td(section))/Lift(section);

disp = double(sdef+mdef); % total tip displacement [m]
%-------------------------------------------------------------------------





