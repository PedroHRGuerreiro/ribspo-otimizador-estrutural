%-------------------------------------------------------------------------
% -- BUCKLING
%-------------------------------------------------------------------------
%    This code sizes wing by static stability analysis headed by
%    NASA curved plate buckling simply supported database.
%    Here just upper leading edge plate can lead to a critical buckling situation.
%    Then this is the only plate studied.
%-------------------------------------------------------------------------
% -- leading edge radius curvature
%-------------------------------------------------------------------------
% Following variables estimate leading edge curvature
x_c             = xx0;
y_c             = yy0;
spl             = spline(x_c,y_c);
fp              = fnder(spl,1);
fpp             = fnder(spl,2);
r               = @(x_c) abs((1 + fnval(fp,x_c).^2).^(3/2)./fnval(fpp,x_c)); % handle function to estimate curvature r
rm              = mean(r(x_c));   % average curvature radious [m]
%-------------------------------------------------------------------------
% -- finding ks/kc
%-------------------------------------------------------------------------
dx              = rib_spacing(section);                                     % plate length [m]
alength         = .25*ch;                                                   % plate width  [m]
ks              = bucklingcoef(dx, alength, td(section), rm, geo.m.nu, geo);% shear buckling coefficient [-]
%-------------------------------------------------------------------------
%% -- Critical stresses
%-------------------------------------------------------------------------
Cb              = 1.3*.6;                                                   % axial stress coefficient [-]
bbi             = min(dx, alength);                                         % actual plate width
sigcr           = Cb*geo.m.Ex*td(section)/rm;                               % critical buckling axial stress
talc            = ks*pi^2*geo.m.Ex*(td(section)/bbi)^2/(12*(1-geo.m.nu^2)); % critical buckling shear stress
ie(section)     = tal(2,1)^2/talc^2 -(sigg(jb-1)/sigcr);                    % if ie>1 this plate's going to suffer buckling
%-------------------------------------------------------------------------
