% Universidade Federal de Uberl�ndia
% Faculdade de Engenharia M�canica
% Curso de Engenharia Aeron�utica
% Ribs spacing analytic topometric optimization (RIBSTO)
% TUCANO AERODESIGN
% Amanda Sousa Martins
% Last edition: 07/16/2020
% ------------------------------------------------------------------------------------
% --------------------------------- B R I E F I N G  --------------------------------- %

% This code aims the optimization of wing/tail mass through rib spacing using boom idealiza-
% tion. It must be used solely to conceptual and preliminary designs. The user must 
% introduce inputs infos on the following files
% 		* All MATLAB input files (don't mind material_library inputs if there's no empi-
% rical data).
% 		* perfil.dat - write just average upper camber airfoil points to estimate wing 
% mass
%       * If the user need to change whether spacing boundary range or ribs number just 
% go to main file and shift varibles n and mini/max.
%  
% ------------------------------------ N O T E S  ------------------------------------ %
% This code uses International System of Units 
% geo is a structure variable that joins together all input infos except perfil.dat
% Ribmass function can be used lonely to estimate wing mass by rib spacing and geo varia-
%ble inputs
% Tip deflection isn't an optimization contraint
% The general coordinate system is
% x : direction of wind flow
% y : direction of spar, oriented to right tip 
% z : direction of lift force 
%
%       --------------------------------------> y
%      
%    |      ---------------------                
%    |     |          |          | 
%    |     |          |          |                
% x \|/     ---------------------      
% 
% Buckling factor = ( critical buckling stress ) / ( current stress )
% ---------------------------------- CODE SUMMARY ------------------------------------ %
% MATLAB Files
% Inputs
	%   wingeometry            - Relate wing geometry properties and save them on variable geo field g
	%   flight_conditions      - Relate flight conditions (velocity, load, factor, etc) and save them on variable geo field f)
	%   bucklingdata           - Read theorical-empirical buckling coefficients for buckling of curved plates 
	%   material_library       - Get material properties and save them on viariable geo field m
% Outputs
	%   vspaero                - Calculate Lift, torsor and bending moment %using solver VSPAERO 
	%   lift_Torsor            - Determine Lift, torsor and bending moment % on each wing section
% Analysis
	%   sizer                  - Determine balsa thickness  and carbon tube radious at each section
	%   deflection 			   - Calculate tip deflection
	%   wingmass 			   - Estimate wing mass 
% Text Files
	%   perfil.dat 			   - Average airfoil to estimate wing mass
	%	output\results.dat 	   - Shows optimization main outputs
%-------------------------------------------------------------------------
tic
fprintf('\n RIBSTO v.1.0 --- Compiled on: %s [-3 GTM] \n',datetime);
%-------------------------------------------------------------------------
% -- Wing inputs
%-------------------------------------------------------------------------
wingeometry
flight_conditions
bucklingdata
material_library
geo.save=0;
%-------------------------------------------------------------------------
% --Initial optimization inputs
%-------------------------------------------------------------------------
function_obj = 'ribmass';                   % Objetive function
crit         = 1;                          
maxi         = .25;                          % max ribs spacing [m]
mini         = .05;                         % min ribs spacing [m]
nribs        = 6;                           % ribs number  
n            = nribs-2;                     % station's number
%-------------------------------------------------------------------------
% -- Optimization Range
%-------------------------------------------------------------------------
for i = 1:n
limx(i,:) = [mini maxi];    % Boundary spacing range [m]
end
n_pop = 5;                  % Pop length (n*vari�veis)
F = 0.8;                    % Mutation factor
CR = 0.8;                   % Crossing factor
iter_max = 10000;           % Max iter number
%-------------------------------------------------------------------------
% -- Optimization Outputs
%-------------------------------------------------------------------------
[ind,otm,iter,bests] = DE(n_pop,F,CR,limx,iter_max,crit,geo);
toc
et=toc;
%-------------------------------------------------------------------------
% -- Recording
%-------------------------------------------------------------------------
tfile = input('\n Como deseja salvar os resultados?\n 0: em results.dat \n 1: em results.xls \n');
writer
%-------------------------------------------------------------------------