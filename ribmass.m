% Universidade Federal de Uberl�ndia
% Faculdade de Engenharia M�canica
% Curso de Engenharia Aeron�utica
% Ribs spacing analytic topometric optimization (RIBSTO)
% T U C A N O  A E R O D E S I G N
% Amanda Sousa Martins
% -------------------------------------------------------------------------
% This code estimate wing weight through boom idealization, taking
% loads from VSPAERO solver.
% -------------------------------------------------------------------------
%
%                 wing mass (f)     = ribmass(ribsp,geo)
%
% ribsp             - rib spacing, except root 
% geo               - inputs geometry(wingeometry.m), material (material_library.m)
% and flight conditions (flight_conditions.m)

% All dimensions in SI units
function f = ribmass(ribsp,geo)
%-------------------------------------------------------------------------
% -- Defining wing
%-------------------------------------------------------------------------
nspa         = length(ribsp)+1;                 % sections number
nribs        = nspa+1;                          % ribs number
%-------------------------------------------------------------------------
% -- Defining wing rib spacing / position
%-------------------------------------------------------------------------
if (sum(ribsp)<=geo.g.bw)                       % Ensures rib spacing sum isn't larger than wingspan
rib_spacing(1) =  DELEqC(geo.g.bw,ribsp,nspa);  % Defines first rib spacing
rib_spacing(2:nspa)   = ribsp;                   

rib_position  = zeros(1,nribs);
rib_position(1)  = 0; 

for i=2:nribs
rib_position(i) = rib_position(i-1) + rib_spacing (i-1)  ;
end
%-------------------------------------------------------------------------
% --Loads & Forces
%-------------------------------------------------------------------------
vspaero
lift_torsor
%-------------------------------------------------------------------------
% -- Tension & sizing
%-------------------------------------------------------------------------
sizer
%-------------------------------------------------------------------------
% -- Mass estimative
%-------------------------------------------------------------------------
wingmass
f = M;
% deflection
fid=fopen('loads-otm.dat','wt');
for i=1:nribs-2
fprintf(fid,'\n Se��o %i: L = %8.2f [N]\n Mx = %8.2f [Nm] \n My = -%8.2f [Nm]\n',i,Lift(i),mx(i),my(i));
end
fclose(fid);


else
    f = 10;
%     fprintf('\n Espa�amentos > semienvergadura: Penaliza��o! excedente = %d \n',sum(ribsp)-geo.g.bw); 
    
end

if geo.save~=0
    
    save('output.mat','sigg','ie','r4','td','talm','Lift','mx','my');
end
                
% fprintf('\nEstimate wing mass: %8.3f [kg]\n',M); 
% deflection

% figure(100)
% bar([f]);
% hold on
