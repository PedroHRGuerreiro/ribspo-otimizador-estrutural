%-------------------------------------------------------------------------
% --Material properties
%-------------------------------------------------------------------------
%  This code read material properties. 
%-------------------------------------------------------------------------
%% -- safety factor
%-------------------------------------------------------------------------
geo.m.FS              = 1.2;                    % Tubo de carbono safety factor [-]
%-------------------------------------------------------------------------
%% --carbon tube - Reference: Tucano n' TSAI,HOA,GAY,. Composite Materials: Design And Applications
%-------------------------------------------------------------------------
% --Assembly properties - considering all plies
% carbon tube composition:
% Vf = 60% fiber volume fraction
% Thickness of each ply: 0.13 mm
%-------------------------------------------------------------------------
geo.m.Xt_carbontube   = 271e6;                  % Laminate tensile strength parallel to the fibre direction [Pa] 
geo.m.Xc_carbontube   = 380e6;                  % Laminate compressive strength parallel to the fibre [Pa]
geo.m.Yt_carbotube    = 580e6;                  % Laminate tensile strength perpendicular to the fibre direction [Pa] 
geo.m.Yc_carbontube   = 809e6;                  % Laminate compressive strength perpendicular to the fibre direction [Pa]               
geo.m.S_carbontube    = 63e6;                   % Laminate shear strength [Pa] 
geo.m.El_carbontube   = 45e9;                   % Young' constant [Pa]
geo.m.Glt_carbontube  = 4.2e9;                  % Coulomb constant [Pa]
geo.m.rho_carbontube  = 1140;                   % laminate density [kg/m3]
% Notes
% All the plies covers the major part of stress at their longitudinal
% direction, but they also support a part of stress resultant in 
% perpendicular direction so
% the following variables give for each ply in the laminate the stresses
% factor along the principal orthotropic directions of the ply, detoned as
% t0 (0� direction) and t9 (90� direction) considering 30% 0� / 70% 90�
% based on Tucano empirical tests
geo.m.sig_l0=2.97*1e6;geo.m.sig_t0=.04*1e6;           % normal stress proportion on 0� plies for 30% 0� / 70% 90�  [Pa]
geo.m.sig_l9=-.02*1e6;geo.m.sig_t9=.15*1e6;           % normal stress proportion on 90� plies for 30% 0� / 70% 90� [Pa]
% SOURCE: TSAI,HOA,GAY,. Composite Materials: Design And Applications
% Appendix 1
%-------------------------------------------------------------------------
%% --carbon fiber - Reference: Tucano n TSAI,HOA,GAY,. Composite Materials: Design And Applications pg. 49
%-------------------------------------------------------------------------
% each ply properties
% Vf = 60% fiber volume fraction
% Thickness of each ply: 0.13 mm
geo.m.Xt_carbonfiber  = 1270e6;                 % carbonfiber tensile strength parallel to the fibre direction [Pa]
geo.m.Xc_carbonfiber  = 1130e6;                 % carbonfiber compressive strength parallel to the fibre [Pa]
geo.m.Yt_carbonfiber  = 42e6;                   % carbonfiber tensile strength perpendicular to the fibre direction [Pa] 
geo.m.Yc_carbonfiber  = 141e6;                  % carbonfiber compressive strength perpendicular to the fibre direction [Pa]               
geo.m.S_carbonfiber   = 63e6;                   % carbonfiber shear strength [Pa] 
geo.m.El_carbonfiber  = 134e9;                  % carbonfiber Young' l direction constant [Pa]
geo.m.Et_carbonfiber  = 7e9;                    % carbonfiber Young' t direction constant [Pa]
geo.m.Glt_carbonfiber = 4.2e9;                  % carbonfiber Coulomb lt constant [Pa]
geo.m.poi_carbonfiber = .25;                    % carbonfiber Poisson lt constant [Pa] 
%-------------------------------------------------------------------------
% -- Hill-Tsai carbon tube criteria factors
%-------------------------------------------------------------------------
% When one detects the rupture of one of the plies, this doesn't
% necessarily lead to the rupture of the whole laminate, but here
% we consider one ply failure isn't tolerated. 
% geo.m.f0*sig_x^2 <1/FSc
% geo.m.f9*sig_x^2  <1/FSc
geo.m.FSc             = 2;                      % carbon tube' safety factor [-]
geo.m.f0              = ((geo.m.sig_l0/geo.m.Xt_carbonfiber)^2 + (geo.m.sig_t0/geo.m.Yt_carbonfiber)^2 + geo.m.sig_l0*geo.m.sig_t0/(geo.m.Xt_carbonfiber)^2)/((1e6)^2);
geo.m.f9              = ((geo.m.sig_l9/geo.m.Xc_carbonfiber)^2 + (geo.m.sig_t9/geo.m.Yt_carbonfiber)^2 + geo.m.sig_l0*geo.m.sig_t9/(geo.m.Xc_carbonfiber)^2)/(1e6)^2;
%-------------------------------------------------------------------------
%% -- Balsa
%-------------------------------------------------------------------------
geo.m.rho_b           = 150;                                        % balsa density [kg/m3]
geo.m.tal_a           =  (.0196*geo.m.rho_b+.3879 )*1e6/geo.m.FS;   % balsa max shear stress [Pa]
geo.m.Ex              =  5.5e9;                                     % balsa elastic constat longitudinal dir. [Pa]
geo.m.Gxy             =  .2e9;                                      % balsa coulomb constant [Pa]
geo.m.nu              =  .006;                                      % balsa poisson factor [-]
%-------------------------------------------------------------------------

