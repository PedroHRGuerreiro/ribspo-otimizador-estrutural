function[ind,result,n_iter,bests] = DE(Q,F,CR,limx,iter_max,crit,geo)
% M�todo de otimiza��o por evolu��o diferencial 
% Igor Henrico - 11511EAR005
% Dimas Silv�rio - 11511EAR028


[n_var,~] = size(limx);         % N�mero de vari�veis de projeto
n_pop = Q*n_var;
pop = zeros(n_pop,n_var);
f = zeros(n_pop,1);
muo = zeros(n_pop,n_var);

fprintf('\n Gerando primeira popula��o \n n_pop: %5.3f \n Fator de cruzamento: %5.3f \n Popula��o: %5.3f \n Fator de muta��o: %5.3f \n',n_pop, F, Q, CR);

if crit == 1
tolerance = 1e-6;               % Toler�ncia para erro relativo do resultado
else
tolerance = 1e-6;    
end
iter = -1;                       % Contador de intera��es
erro = 1;                       % Erro inicial
%% C�lculo da popula��o inicial

for ii = 1:n_var
pop(:,ii) = limx(ii,1) + rand(1,n_pop).*(limx(ii,2)- limx(ii,1));
end

% for ii = 1:n_var/2
% pop(:,ii) = ones(1,n_pop).*5;
% pop(:,ii+5) = ones(1,n_pop).*40;
% end

%% Avalia��o dos resultados da popula��o inicial na fun��o objetivo

for ii = 1:n_pop
f(ii) = feval(@ribmass,pop(ii,:), geo);
end
[~, I] = min(f);
% bests(1,:) = [pop(I,:) best];                       % Melhores indiv�duos

while erro >= tolerance
    if iter >= iter_max
        break
    end
fprintf('\n Itera��o em an�lise: %5.0f \n',iter);

    
%% Estrat�gia de cruzamento --> DE/BEST/1
mui = rand(n_pop,n_var);
a = randperm(n_pop);
b = randperm(n_pop);
for ii = 1:n_pop
    for jj = 1:n_var
        if mui(ii,jj) < CR
            mui(ii,jj) = 1;
            muo(ii,jj) = 0;
        else 
            mui(ii,jj) = 0;
            muo(ii,jj) = 1;
        end
    end
end
xp1 = pop(a,:);
xp2 = pop(b,:);
xn = pop(I,:)+F*(xp1 - xp2);
popp = pop.*muo+xn.*mui;                                % Pr�xima popula��o

%% Teste dos limites laterais
for ii = 1:n_pop
    for jj = 1:n_var
        if popp(ii,jj) <= limx(jj,1)
            popp(ii,jj) = limx(jj,1);
        elseif popp(ii,jj) >= limx(jj,2)
            popp(ii,jj) = limx(jj,2);
        end
    end
end
    
%% Avalia��o dos resultados da popula��o n na fun��o objetivo
for ii = 1:n_pop
f(ii) = feval(@ribmass,popp(ii,:), geo);
end
[best, I] = min(f);

%% Finaliza��o 
iter = iter+1;
count(iter+1) = iter;
bests(iter+1,:) = [best,popp(I,:)];                       % Melhores indiv�duos


pop = popp;
if crit == 1
if iter>=4
    erro1 = abs((bests(iter-2,1)-bests(iter-3,1))/bests(iter+1,1));
    erro2 = abs((bests(iter-1,1)-bests(iter-2,1))/bests(iter+1,1));
    erro3 = abs((bests(iter,1)-bests(iter-1,1))/bests(iter+1,1));
    erro4 = abs((bests(iter+1,1)-bests(iter,1))/bests(iter+1,1));
    erro = mean([erro1,erro2,erro3,erro4]);
end
else
erro = mean(std(pop));
end

fprintf('\n --------------------------------- \n');
fprintf('\n Itera��o: %5.0f \n Massa: %1.4f[kg]\n erro: %1.15f\n Posicionamento: ',iter-1,best,erro);
[geo.g.bw-sum(pop(ii,:)) pop(ii,:)]
fprintf('\n --------------------------------- \n');
plot(0:iter,bests(:,1))
hold on

end
[result,I] = min(bests(:,1));
ind = bests(I,:);
ind(1) = [];
n_iter = iter;
figure
plot(0:iter,bests(:,1))
hold on
grid
legend('Resultado m�nimo')
xlabel('Itera��es')
title('Converg�ncia de vari�veis e resultado')
save iter ind result
% system('shutdown -s')
end