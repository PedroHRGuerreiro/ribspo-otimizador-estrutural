if tfile==0
load('output.mat')

fid=fopen('results.dat','wt');

fprintf(fid, '------------------- R I B S T O   R E S U L T S -------------------  \n \n');
fprintf(fid, '\n RIBSTO v.1.0 --- Dat file wrote at: %s [-3 GTM] \n \n \n',datetime);


fprintf(fid, '--- Wing geometry \n \n');


    fprintf(fid, 'Sections number: %6.0f \n',geo.g.ns);
    fprintf(fid, 'Root chord:      %10.3f [m]\n',geo.g.cr);
    fprintf(fid, 'Tip chord:       %10.3f [m]\n',geo.g.ct);
    fprintf(fid, 'Semi wing span:  %10.3f [m]\n',geo.g.bw);
    for i=1:geo.g.ns+1
    fprintf(fid, 'Airfoil section %1.0f:    %s    \n',i-1,geo.g.airfoil{i});
    end
    fprintf(fid, '\n');


fprintf(fid, '--- Flight conditions \n \n');


    fprintf(fid, 'Cruise speed:    %7.0f [m/s] \n',geo.fc.vc);
    fprintf(fid, 'AOA:             %7.0f [�]\n',geo.fc.AOA);
    fprintf(fid, 'MACH:            %10.3f  \n',geo.fc.Mc);
    fprintf(fid, 'Load factor:    %7.0f  \n',geo.fc.FC);
    fprintf(fid, 'MTOW:           %10.1f [kg]\n',geo.fc.mtow);
    
    fprintf(fid, '\n');
    
    fprintf(fid, '--- Loads \n \n');
    
    fprintf(fid, 'Total Lift:    %7.2f [N] \n',sum(Lift)*2);
    fprintf(fid, 'Total Torsor Moment:    %7.2f [Nm] \n',sum(my)*2);
    fprintf(fid, 'Total Bending Moment:    %7.2f [Nm] \n',sum(mx)*2);

    
    fprintf(fid, '\n');
    
    

 fprintf(fid, '--- Diferential Evolution Optimization parameters \n \n');

% 
    fprintf(fid, 'Max rib spacing: %10.3f [m] \n',maxi);
    fprintf(fid, 'Min rib spacing: %10.3f [m]\n',mini);
    fprintf(fid, 'Ribs number:     %6.0f  \n',nribs);
    fprintf(fid, 'Sections number: %6.0f  \n',n);
    fprintf(fid, 'Pop. number:     %6.0f \n',n_pop);
    fprintf(fid, 'Mutation factor:   %6.1f \n',F);
    fprintf(fid, 'Crossing factor:   %6.1f \n',CR);
        fprintf(fid, 'Elapsed time:        %6.2f [min]\n',et/60);
%     
geo.save=1;    

otm = ribmass(ind,geo);
ind=[geo.g.bw-sum(ind) ind];

 fprintf(fid, '\n--- Best wing \n \n');

    fprintf(fid, '\nWing mass:       %10.3f [kg]\n',otm);
    fprintf(fid, 'Total iterations number:  %2.0f  \n',iter);
    fprintf(fid, 'Best rib spacing: [m] ');
    fclose(fid);
    dlmwrite('results.dat',ind,'-append',...
 'delimiter','\t','precision',4);


    n=nribs-1;
    Max_Axial_Stress = abs(sigg(1:n));
    Max_Shear_Stress = talm(1:n)';
    Buckling_factor = ie(1:n).^-1;
    Carbontube_Radious = r4(1:n);
    Balsa_thickness = td(1:n);
     

sec = 1:n;
Section = sec';

% 
clc
    fprintf('\n--------------------------------- R E S U L T S ---------------------------------------  \n \n');

    
        fprintf('\n BEST WING  \n \n');

    fprintf('\nWing mass:       %10.3f [kg]\n',otm);
    fprintf('Total iterations number:  %2.0f  \n',iter);
    fprintf('Best rib spacing: [m] \n');
    ind
    
            fprintf('\n ---- DETAILS IN results file \n \n');

    

    
 fod = fopen('results.dat','a+'); 
 
 fprintf(fod,'\n Load Distribution: \n \n');
fprintf(fod, '\n %6s      \t%6s            \t%6s       \t%6s \n', 'Section', 'Lift', 'Bend. Moment',...
    'Torsor Moment');

for i=1:n
    
fprintf(fod, ' \n %3d   \t%15.1f   \t%15.1f \t%15.1f \n \n', Section(i), Lift(i),...
    mx(i),my(i));

end
%---
 
 fprintf(fod,'\n Sections max stresses and sizing result: \n \n');
fprintf(fod, '\n %6s \t%6s \t%6s \t%6s \t%6s \t%6s\n \n \n ', 'Section', 'Max_Axial_Stress', 'Max_Shear_Stress',...
    'Buckling_factor','Carbontube_Radious'...
    ,'Balsa_thickness');

for i=1:n
    
fprintf(fod, ' \n %3d   \t%15.1e   \t%15.1e \t%15.1e \t%15.1e \t%15.1e \n \n', Section(i), Max_Axial_Stress(i), Max_Shear_Stress(i)...
    ,Buckling_factor(i),Carbontube_Radious(i),Balsa_thickness(i));

end

 


   
    fprintf(fod, '\n------------------------------------------------- E N D -----------------------------------------------------');



        
fclose(fod);
system('del.bat &');



% ----------------------------
% 




else



load('output.mat')

fid=fopen('results.xls','wt');

fprintf(fid, '------------------- R I B S T O   R E S U L T S -------------------  \n \n');
fprintf(fid, '\n RIBSTO v.1.0 --- Dat file wrote at: %s [-3 GTM] \n \n \n',datetime);


fprintf(fid, 'Wing geometry \n \n');


    fprintf(fid, 'Sections number: %6.0f \n',geo.g.ns);
    fprintf(fid, 'Root chord:      %10.3f [m]\n',geo.g.cr);
    fprintf(fid, 'Tip chord:       %10.3f [m]\n',geo.g.ct);
    fprintf(fid, 'Semi wing span:  %10.3f [m]\n',geo.g.bw);
    for i=1:geo.g.ns+1
    fprintf(fid, 'Airfoil section %1.0f:    %s    \n',i-1,geo.g.airfoil{i});
    end
    fprintf(fid, '\n');


fprintf(fid, 'Flight conditions \n \n');


    fprintf(fid, 'Cruise speed:    %7.0f [m/s] \n',geo.fc.vc);
    fprintf(fid, 'AOA:             %7.0f [�]\n',geo.fc.AOA);
    fprintf(fid, 'MACH:            %10.3f  \n',geo.fc.Mc);
    fprintf(fid, 'Load factor:    %7.0f  \n',geo.fc.FC);
    fprintf(fid, 'MTOW:           %10.1f [kg]\n',geo.fc.mtow);
    
    fprintf(fid, '\n');
    
    fprintf(fid, 'Loads \n \n');
    
    fprintf(fid, 'Total Lift:    %7.2f [N] \n',sum(Lift)*2);
    fprintf(fid, 'Total Torsor Moment:    %7.2f [Nm] \n',sum(my)*2);
    fprintf(fid, 'Total Bending Moment:    %7.2f [Nm] \n',sum(mx)*2);

    
    fprintf(fid, '\n');
    
    

 fprintf(fid, 'Diferential Evolution Optimization parameters \n \n');

% 
    fprintf(fid, 'Max rib spacing: %10.3f [m] \n',maxi);
    fprintf(fid, 'Min rib spacing: %10.3f [m]\n',mini);
    fprintf(fid, 'Ribs number:     %6.0f  \n',nribs);
    fprintf(fid, 'Sections number: %6.0f  \n',n);
    fprintf(fid, 'Pop. number:     %6.0f \n',n_pop);
    fprintf(fid, 'Mutation factor:   %6.1f \n',F);
    fprintf(fid, 'Crossing factor:   %6.1f \n',CR);
        fprintf(fid, 'Elapsed time:        %6.2f [min]\n',et/60);
%     
geo.save=1;    

otm = ribmass(ind,geo);
ind=[geo.g.bw-sum(ind) ind];

 fprintf(fid, '\nBest wing \n \n');

    fprintf(fid, '\nWing mass:       %10.3f [kg]\n',otm);
    fprintf(fid, 'Total iterations number:  %2.0f  \n',iter);
    fprintf(fid, 'Best rib spacing: [m] ');
    fclose(fid);
    dlmwrite('results.xls',ind,'-append',...
 'delimiter','\t','precision',4);


    n=nribs-1;
    Max_Axial_Stress = abs(sigg(1:n));
    Max_Shear_Stress = talm(1:n)';
    Buckling_factor = ie(1:n).^-1;
    Carbontube_Radious = r4(1:n);
    Balsa_thickness = td(1:n);
     

sec = 1:n;
Section = sec';

% 
clc
    fprintf('\n--------------------------------- R E S U L T S ---------------------------------------  \n \n');

    
        fprintf('\n BEST WING  \n \n');

    fprintf('\nWing mass:       %10.3f [kg]\n',otm);
    fprintf('Total iterations number:  %2.0f  \n',iter);
    fprintf('Best rib spacing: [m] \n');
    ind
    
            fprintf('\n ---- DETAILS IN results file \n \n');

    

    
 fod = fopen('results.xls','a+'); 
 
 fprintf(fod,'\n Load Distribution: \n \n');
fprintf(fod, '\n %6s      \t%6s            \t%6s       \t%6s \n', 'Section', 'Lift', 'Bend. Moment',...
    'Torsor Moment');

for i=1:n
    
fprintf(fod, ' \n %3d   \t%15.1f   \t%15.1f \t%15.1f \n \n', Section(i), Lift(i),...
    mx(i),my(i));

end
%---
 
 fprintf(fod,'\n Sections max stresses and sizing result: \n \n');
fprintf(fod, '\n %6s \t%6s \t%6s \t%6s \t%6s \t%6s\n \n \n ', 'Section', 'Max_Axial_Stress', 'Max_Shear_Stress',...
    'Buckling_factor','Carbontube_Radious'...
    ,'Balsa_thickness');

for i=1:n
    
fprintf(fod, ' \n %3d   \t%15.1e   \t%15.1e \t%15.1e \t%15.1e \t%15.1e \n \n', Section(i), Max_Axial_Stress(i), Max_Shear_Stress(i)...
    ,Buckling_factor(i),Carbontube_Radious(i),Balsa_thickness(i));

end

 


   
    fprintf(fod, '\n------------------------------------------------- E N D -----------------------------------------------------');



        
fclose(fod);
system('del.bat &');


end
% ----------------------------
% 






