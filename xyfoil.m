function [x0,y0,x,y]=xyfoil(section, geo, rib_position)

if rib_position(section)<=geo.g.bs(2)*.5
    perfil = geo.g.airfoil{1};
elseif rib_position(section)>geo.g.bs(2)*.5 && rib_position(section)<=geo.g.bs(3)*.5
    perfil = geo.g.airfoil{2};
    elseif rib_position(section)>geo.g.bs(3)*.5 %&& rib_position(section)<=geo.g.bs(4)*.5
    perfil = geo.g.airfoil{3};
%     elseif rib_position(section)>geo.g.bs(4)*.5
%     perfil = geo.g.airfoil{4};
end

coord=load(perfil);
x=coord(1:length(coord),1); 
y=coord(1:length(coord),2);
% xx = [.05 .25 .75];
% yy = spline(x,y,xx);
% % 
% xxx=[xx flip(xx)];
% yyy=[yy -flip(yy)];
% plot(xxx,yyy);
x0=[0.0500    0.0500    0.2500    0.2500    0.7500    0.7500];

ah=find(y<0,1);
ab=find(x(ah:end)>=.05,1);
y1 = y(ah+ab-1);

y2 = y(find(x<=.05,1));

ah=find(y<0,1);
ab=find(x(ah:end)>=.25,1);
y3 = y(ah+ab-1);

y4 = y(find(x<=.25,1));

ah=find(y<0,1);
ab=find(x(ah:end)>=.75,1);
y5 = y(ah+ab-1);

y6 = y(find(x<=.75,1));

% 
y0=[y1 y2 y3 y4 y5 y6];
 
% 
%  plot(x0,y0,'ob')
% 
% a=20;


end


