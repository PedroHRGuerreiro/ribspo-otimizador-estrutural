%-------------------------------------------------------------------------
% --VSPAERO
%-------------------------------------------------------------------------
%  This code runs VSP_AERO and imports wing load to matlab.
%-------------------------------------------------------------------------
%-------------------------------------------------------------------------
% --Writing wing geometry
%-------------------------------------------------------------------------
fid = fopen('wingmodel.as','wt');
fprintf(fid, 'void main() \n { \n string wid = AddGeom( "WING", "" ); \n');
for i=1:length(geo.g.ssb)
    if i~=length(geo.g.ssb)
        fprintf(fid, 'InsertXSec( wid, 1, XS_FOUR_SERIES );\n');
        
    end
    
    fprintf(fid, 'SetParmVal( wid, "Root_Chord", "XSec_%i", %f );\n',i,geo.g.ccb(i));
    fprintf(fid, 'SetParmVal( wid, "Tip_Chord", "XSec_%i", %f );\n',i,geo.g.ccb(i+1));
    fprintf(fid, 'SetParmVal( wid, "Area", "XSec_%i", %f );\n',i,geo.g.ssb(i));
    fprintf(fid, 'SetParmVal( wid, "Span", "XSec_%i", %f );\n',i,geo.g.bss(i));
    fprintf(fid, 'SetParmVal( wid, "geo.g.twist","XSec_%i", %f );\n',i,geo.g.twist(i));
    fprintf(fid, 'SetParmVal( wid, "Sweep","XSec_%i", %f );\n Update();\n',i,0);
    
    fprintf(fid, ['string xsec_surf%i = GetXSecSurf( wid, %i );\n', ...
        'ChangeXSecShape( xsec_surf%i, %i, XS_FILE_AIRFOIL );\n', ...
        'string xsec%i = GetXSec( xsec_surf%i, %i );\n',...
        'ReadFileAirfoil( xsec%i, "%s" );\n',...
        'array< vec3d > @up_array%i = GetAirfoilUpperPnts( xsec%i );\n',...
        'array< vec3d > @low_array%i = GetAirfoilLowerPnts( xsec%i );\n',...
        'SetAirfoilPnts( xsec%i, up_array%i, low_array%i );\n Update();\n'],i,i,i,...
        i,i,i,i,i,geo.g.airfoil{i+1},...
        i,i,i,i,i,i,i);
    
end

airf = char(geo.g.airfoil{i+1});
fprintf(fid, ['string xsec_surf%i = GetXSecSurf( wid, %i );\n', ...
    'ChangeXSecShape( xsec_surf%i, %i, XS_FILE_AIRFOIL );\n', ...
    'string xsec0 = GetXSec( xsec_surf%i, %i );\n',...
    'ReadFileAirfoil( xsec%i, "%s" );\n',...
    'array< vec3d > @up_array%i = GetAirfoilUpperPnts( xsec%i );\n',...
    'array< vec3d > @low_array%i = GetAirfoilLowerPnts( xsec%i );\n',...
    'SetAirfoilPnts( xsec%i, up_array%i, low_array%i ); \n Update();'],0,0,0,0,0,0,0,airf,...
    0,0,0,0,0,0,0);

fprintf(fid, 'Update();\n WriteVSPFile( "Test001.vsp3", SET_ALL );\n SetComputationFileName( DEGEN_GEOM_CSV_TYPE, "wingmodel_DegenGeom.csv" );\n ComputeDegenGeom( SET_ALL, DEGEN_GEOM_CSV_TYPE );\n }');
fclose(fid);
%-------------------------------------------------------------------------
% --Making geometry file
%-------------------------------------------------------------------------
command01  = 'vsp -script wingmodel.as';
status = dos(command01);
%-------------------------------------------------------------------------
% -- Running VSPAERO
%-------------------------------------------------------------------------
% VSP AERO TEST
% Declare name of vsp file to run
modelname = 'wingmodel';
%% Setup VSPaero conditions file
% min/max mach number, and number of points between to run
% MinMach = 0.1;
% MaxMach = 0.8;
% machdiv = 4;
% mach = linspace(MinMach,MaxMach,machdiv);
% min/max Angle of Attack, and number of points between to run
% MinAoA = 0;
% MaxAoA = 6;
% AoAdiv = 4;
% AOA = linspace(MinAoA,MaxAoA,AoAdiv);
wakeiters = 3;
Aerodata = [];
cbar        = 2/3*geo.g.cr*(1+geo.g.lamb+geo.g.lamb^2)/(1+geo.g.lamb);
% Adjust VSPaero inputs here (density etc.)
Sref = num2str(2*geo.g.s);
Cref = num2str(cbar);
Bref = num2str(2*geo.g.bw);
X_cg = '0.00';
Y_cg = '0.000000';
Z_cg = '0.000000';
Mach = num2str(geo.fc.Mc);
AoA = num2str(geo.fc.AOA);
Beta = '0.000000';
Vinf = num2str(geo.fc.vc);
Rho = num2str(geo.fc.rho);
ReCref = '100000000.000000';
ClMax = '-1.000000';
Symmetry = 'No';
FarDist = '-1.000000';
NumWakeNodes = '0';
WakeIters = num2str(wakeiters);
vspaerofile = [modelname,'_DegenGeom.VSPAERO'];
fid = fopen(vspaerofile,'wt');
% adding string "values" above to definition strings for vspaero text file.
a1 = ['Sref = ',Sref];
a2 = ['Cref = ',Cref];
a3 = ['Bref = ',Bref];
a4 = ['X_cg = ',X_cg];
a5 = ['Y_cg = ',Y_cg];
a6 = ['Z_cg = ',Z_cg];
a7 = ['Mach = ',Mach];
a8 = ['AoA = ',AoA];
a9 = ['Beta = ',Beta];
a10 = ['Vinf = ',Vinf];
a11 = ['Rho = ',Rho];
a12 = ['ReCref = ',ReCref];
a13 = ['ClMax = ',ClMax];
a14 = ['Symmetry = ',Symmetry];
a15 = ['FarDist = ',FarDist];
a16 = ['NumWakeNodes = ',NumWakeNodes];
a17 = ['WakeIters = ',WakeIters];
% write out .vspaero text file (required to run vspaero per case)
fprintf( fid, '%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n', a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16,a17);
fclose(fid);

% Run VSPaero via the windows cmd console
% -omp N designates N processors to vsp aero.

% Make sure to change file name below to match model being run.
command = 'vspaero -omp 4 wingmodel_DegenGeom';
evalc('status = dos(command)');
% system('del.bat &');
%-------------------------------------------------------------------------
% -- Outputs VSPAERO
%-------------------------------------------------------------------------
Aerodata=importdata('wingmodel_DegenGeom.lod');
yya=Aerodata.data(:,3);
Cm=Aerodata.data(:,13);
Cl=Aerodata.data(:,6);
%-------------------------------------------------------------------------

