%-------------------------------------------------------------------------
% --Wing geometric informations
%-------------------------------------------------------------------------
%  This code read wing geometry features. The vector variables are oriented
%  from root [position 1] to tip [position ns]
%-------------------------------------------------------------------------
geo.g.ns          = 2;                         % number of wing sections
geo.g.AR          = [1.533 1.682];             % each section aspect ratio [-]
geo.g.cr          = .3543;                     % root chord         [m]
geo.g.ct          = 0.1771;                    % tip chord          [m]
geo.g.lamb        = geo.g.ct/geo.g.cr;         % taper ratio        [-]
%geo.g.FS          = 1.5;                      % safety factor      [-]
geo.g.bw          = 0.99;                      % half spanwise      [m]
geo.g.sw          = [0 0];                     % each section sweep angle   [�]
geo.g.s           = .6223*.5;                  % one-half reference area    [m�]
geo.g.ccb         = [0.3543 0.3543 0.1771];    % each section first chord[m]
geo.g.ssb         = [0.1924 0.1188];           % each section area       [m�]
geo.g.bs          = [0 0.5431 0.99];     % each section y position [m]
geo.g.bss         = diff(geo.g.bs);               % each section length     [m] 
% positive twist angle increases angle if atack
geo.g.twist       = [-2 4 1];               % twist angle [�]
% all airfoil files must be in code directory, put their name on the
% following variable
geo.g.airfoil     = {'BA7-S1223.dat';'BA7-S1223.dat';'WTU-01.dat'};  % airfoil files
%-------------------------------------------------------------------------