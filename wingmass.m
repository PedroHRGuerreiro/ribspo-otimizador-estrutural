%-------------------------------------------------------------------------
% -- wingmass
%-------------------------------------------------------------------------
%    Estimate wing mass
%-------------------------------------------------------------------------
%
%
% RAIZ                 Ponta
%                     <---
%                      z
% All variables're related to a wing semispan
%% DADOS DE MATERIAL %%

dens_honey = 169;              % [Kg/m�] Densidade do honey
bonder     = 0.005;            % [Kg] Massa de bonder por frasco
fita       = 0.044757;         % [Kg/m�] Densidade por �rea de asa de entelagem de fita
cov        = 0.025575;         % [Kg/m�] Densidade por �rea de asa de entelagem de coverite
fio        = 0.01;             % [Kg/m] Densidade linear de fia��o
servo      = 0.015;            % [Kg] Massa de servo com casinha

%% C�LCULO DAS �REAS E COMPRIMENTOS DOS PERFIS %%
%% �REAS ENTELAGEM %%
coord  = load('perfil.dat');
x=coord(:,1);
y=coord(:,2);
lab=find(x<.25,1);

perf = abs([x y]);
yb = linspace(0,geo.g.bw,nribs);
perfis_asa = zeros(length(x),2,nribs);

for i=1:nribs
    perfis_asa(:,:,i)=2*ccr(i)*perf; % Vetor com as cordenadas reais dos perfis
    comp_perf(i)=sum(sqrt(diff(perfis_asa(:,1,i)).^2+diff(perfis_asa(:,2,i)).^2)); % Comprimento do perfil
    area_perf(i)=polyarea(perfis_asa(:,1,i),perfis_asa(:,2,i)); % �rea do perfil
end


comp_nerv = sum(comp_perf)-comp_perf(round(nribs/2))-comp_perf(round(nribs/2)+1); % Somat�rio dos comprimentos dos perfis
area_nerv = sum(area_perf)-area_perf(round(nribs/2))-area_perf(round(nribs/2)+1); % Somat�rio das �reas dos perfis de balsa
area_honey = area_perf(round(nribs/2))+area_perf(round(nribs/2)+1); % Somat�rio das �reas dos perfis de honey RAIZ
area_ent=(comp_perf(1:length(comp_perf)-1)+comp_perf(2:length(comp_perf))).*diff(yb)/2; % �reas de intra e extradorso
area_ent=sum(area_ent); % Somat�rio da �rea de entelagem

%% COMPRIMENTO DO BORDO DE FUGA %%
comp_fuga = zeros(1,nribs-1);
for i=1:nribs-1
comp_fuga(i) = sqrt((flip(rib_spacing(i)))^2 + (0.75*ccr(i)-0.75*ccr(i+1))^2); % Comprimento total do bordo
end
%% �REA DO BORDO DE ATAQUE %%
area_casca = zeros(1,nribs-1);
for i=1:nribs-1
area_casca(i) = sum((diff(perfis_asa(lab:length(x),1,i)).^2 + diff(perfis_asa(lab:length(x),2,i).^2)))*flip(rib_spacing(i));
end
area_long = zeros(1,nribs-1);
area_long(1) = (sqrt((perfis_asa(lab,1,1)-perfis_asa(length(x),1,1))^2+(perfis_asa(lab,2,1)-perfis_asa(length(x),2,1))^2)+...
    sqrt((perfis_asa(lab,1,2)-perfis_asa(length(x),1,2))^2+(perfis_asa(lab,2,2)-perfis_asa(length(x),2,2))^2))*...
    (yb(2)-yb(1))*0.5; % �rea da segunda longarina

for i=2:nribs-1
    area_long(i) = (sqrt((perfis_asa(lab,1,i)-perfis_asa(length(x),1,i))^2+(perfis_asa(lab,2,i)-perfis_asa(length(x),2,i))^2)+...
    sqrt((perfis_asa(lab,1,i+1)-perfis_asa(length(x),1,i+1))^2+(perfis_asa(lab,2,i+1)-perfis_asa(length(x),2,i+1))^2))*...
    (yb(i+1)-yb(i))*0.5; % �rea da segunda longarina
end
area_bordo = zeros(1,nribs-1);
for i=1:nribs-1
area_bordo(i) = area_casca(i)+area_long(i);
end
 % �rea total do bord

%% VOLUME DE BALSAS %%
vol_nerv=0;
vol_fuga=0;
vol_capstrike=0;
vol_bordo=0;

for i=1:nribs-1
vol_nerv = vol_nerv + area_perf(i)*flip(td(i)); % Volume de balsa das nervuras
vol_fuga = vol_fuga+0.1*ccr(i)*(4*sqrt((flip(rib_spacing(i)))^2 + (0.75*ccr(i)-0.75*ccr(i+1))^2))*flip(td(i)); % Volume de balsa do bordo
vol_capstrike = vol_capstrike+0.005*(0.67*comp_perf(i))*flip(td(i)); % Volume de balsa dos capstrikes
vol_bordo = vol_bordo+area_bordo(i)*flip((td(i))); % Volume do bordo
end
vol_vareta = 0.006*0.003*sum(comp_fuga); % Volume das varetas de balsa para aileron e flap

%% VOLUME DE HONEY %%

vol_honey = area_honey*0.008; % Volume de honey

%% TUBO DE CARBONO %%
area_tubo=zeros(1,nribs-1);
vol_tubo=zeros(1,nribs-1);

for i=1:nribs-1
    area_tubo(i) =  pi*(r3(i)^2-(r3(i)-tc)^2)+pi*(r4(i)^2-(r4(i)-tc)^2); % [m�] �rea do tubo
    vol_tubo(i)  =  area_tubo(i)*rib_spacing(i); % [m�] Volume do tubo
end

vol_tubo=sum(vol_tubo);
%  Rtubo = 0.001;
%  ttubo = 0.0005;
% Rtubo = Rtubo/1000; % [m] Raio externo do tubo
% ttubo = ttubo/1000; % [m] Espessura do tubo
% area_tubo = pi*(Rtubo^2-(Rtubo-ttubo)^2); % [m�] �rea do tubo
% vol_tubo = area_tubo*bw*2; % [m�] Volume do tubo

%% MASSAS %%

M_tubo = 2*vol_tubo*geo.m.rho_carbontube; % Massa do tubo de carbono
M_nerv = 2*vol_nerv*geo.m.rho_b; % Massa de nervuras
M_fuga = 2*vol_fuga*geo.m.rho_b; % Massa do bordo de fuga
M_capstrike = 2*vol_capstrike*geo.m.rho_b; % Massa dos capstrikes
M_vareta = 2*vol_vareta*geo.m.rho_b; % Massa das varetas
M_bonder = 6*bonder; % Massa de superbonder
M_honey = 2*vol_honey*dens_honey; % Massa de honey
M_ent = 2*area_ent*cov; % Massa de coverite
M_bordo = 2*vol_bordo*geo.m.rho_b; % Massa do bordo de ataque
M_elet = (0.8+0.6)*geo.g.bw*2*fio+4*servo; % Massa el�trica
M = M_tubo+M_nerv+M_fuga+M_capstrike+M_vareta+M_bonder+M_honey+M_ent+M_bordo+M_elet;
T=table(M_tubo,M_nerv,M,'VariableNames',{'Tubo','Nerv','Total'});
T=table(M_tubo,M_nerv,M_fuga,M_capstrike,M_vareta,M_bonder,M_honey,M_ent,M_bordo,M_elet,M,'VariableNames',{'Tubo','Nerv','Fuga','capstrike','Vareta','Bonder','honey','Ent','bordo','Elet','Total'});

% keyboard;

%% PLOTAGEM DA ASA %%

% figure(2)
% for i=1:nribs
%     perfis_asa2(:,i,1:2)=ccr(i)*perf; % Apenas para plotagem da asa
%     perfis_asa2(:,i,3)=yb(i)*ones(length(perf),1); % Apenas para plotagem da asa
% end
% 
% surf(perfis_asa2(:,:,3),perfis_asa2(:,:,1),perfis_asa2(:,:,2))
% axis equal
