n = input('quantas nervuras tem a asa?: ');
ns = n - 1;
load('output.mat')
load('iter.mat')
grid on
grid minor 
axis ( [0 n 0 5*10^-3]) 
fprintf('dimens�o do tubo de carbono por se��o: ');
r4
plot([1:ns],r4.*1000,'ok','DisplayName','raio do tubo de carbono [mm]')
hold on
fprintf('espessura da balsa por se��o: ');
td
plot([1:ns],td.*1000,'o-b','DisplayName','espessura da balsa [mm]')
grid on
grid minor 
axis ( [0 n 0 5]) 
xlabel ('Se��es (intervalos entre nervuras)')
ylabel ('Espessura da balsa e raio do tubo [mm] ')
