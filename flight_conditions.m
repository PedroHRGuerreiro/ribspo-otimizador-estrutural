%-------------------------------------------------------------------------
% --Flight conditions
%-------------------------------------------------------------------------
%  This code read critical aircraft flight condition. 
%-------------------------------------------------------------------------
geo.fc.h           = 70;                        % flight altitute            [m]
geo.fc.vc          = 23;                        % cruise speed              [m/s]
geo.fc.AOA         = 0;                        % angle of attack            [�]
[~,a,~,rho]     = atmosisa(geo.fc.h);           % sound speed at fligth alt. [m/s]
geo.fc.a           = a;                         
geo.fc.rho         = rho;
geo.fc.Mc          = geo.fc.vc/geo.fc.a;        % Mach number of cruise speed [-]
geo.fc.q           = .5*geo.fc.rho*geo.fc.vc^2; % dynamic pressure            
geo.fc.FC          = 1;                         % load factor                 [-]
geo.fc.mtow        = 13.70;                      % max take off weight         [kg]
geo.fc.g           = 9.81;                      % gravity acceleration       [m/s�]
%-------------------------------------------------------------------------